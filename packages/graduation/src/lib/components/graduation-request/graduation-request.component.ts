import { Component, OnInit, ViewContainerRef, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { GraduationRequestService } from './../../services/graduation-request/graduation-request.service';
import { GraduationRequestStatus } from './../../graduation';
import { GraduationRequestWizardDirective } from './../../directives/graduation-request-step-directive';

@Component({
  selector: 'universis-graduation-request',
  templateUrl: './graduation-request.component.html'
})
export class GraduationRequestComponent implements OnInit {
  // This component is based on this cookbook example
  // https://angular.io/guide/dynamic-component-loader

  @ViewChild(GraduationRequestWizardDirective) GraduationRequestWizard: GraduationRequestWizardDirective;

  public wizardSteps: any;
  public activeTemplate: any;
  public currentAdIndex = -1;
  public activeStep;

  constructor(
    private _graduationRequestService: GraduationRequestService,
    private _componentFactoryResolver: ComponentFactoryResolver,
    public viewContainerRef: ViewContainerRef
  ) { }

  public graduationRequestStatus: GraduationRequestStatus;

  async ngOnInit() {
    const fakeStudentId = 1;
    try {
      this.graduationRequestStatus = await this._graduationRequestService.getGraduationRequestStatus(fakeStudentId);
      console.log(this.graduationRequestStatus);
      this.loadComponent(0);
    } catch (err) {
      console.error(err);
    }
  }

  /**
   *
   * Shows the component (step) at the wizard
   *
   * @param index The index inside the steps array to show
   *
   */
  loadComponent(index: number): void {

    if (this.graduationRequestStatus.steps[index].status === 'unavailable') {
      return;
    }

    const component = this.graduationRequestStatus.steps[index].component.component;
    this.activeStep = this.graduationRequestStatus.steps[index];
    this.currentAdIndex = index;
    const componentFactory = this._componentFactoryResolver.resolveComponentFactory(component);
    const viewContainerRef = this.GraduationRequestWizard.viewContainerRef;
    viewContainerRef.clear();
    viewContainerRef.createComponent(componentFactory);
  }
}
