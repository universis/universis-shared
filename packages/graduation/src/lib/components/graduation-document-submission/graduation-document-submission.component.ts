import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'universis-graduation-document-submission',
  templateUrl: './graduation-document-submission.component.html'
})
export class GraduationDocumentSubmissionComponent implements OnInit {

  public documents = [
    {
      alternateName: 'libraryDocument',
      title: 'Confirmation form department\'s library',
      description: `Lorem ipsum dolor sit amet, do eiusmod tempor incididunt labore
                    et dolore magna aliqua.`,
      documentUrl: 'https://www.sample-videos.com/text/Sample-text-file-10kb.txt',
      status: 'complete',
      messages: [{
        from: 'student',
        dateCreated: new Date(),
        contents: 'This is a message from student'
      }, {
        from: 'library service',
        dateCreated: new Date(),
        contents: 'This is a message from the service'
      }
    ]
    },
    {
      alternateName: 'academicId',
      title: 'Academic Id',
      description: `Lorem ipsum dolor sit amet, do eiusmod tempor incididunt labore
                    et dolore magna aliqua.`,
      documentUrl: 'https://www.sample-videos.com/text/Sample-text-file-10kb.txt',
      status: 'pending',
      messages: []
    },
    {
      alternateName: 'studentCare',
      title: 'Confirmation of removal from student care',
      description: `Lorem ipsum dolor sit amet, do eiusmod tempor incididunt labore
                    et dolore magna aliqua.`,
      documentUrl: null,
      status: 'fail',
      messages: []
    }
  ];

  constructor() { }

  public messageBlocksCollapseState: Array<Boolean>;

  ngOnInit() {
    this.messageBlocksCollapseState = new Array(this.documents.length).fill(true);
  }

  public collapseMessageBlock(index: number) {
    this.messageBlocksCollapseState[index] = !this.messageBlocksCollapseState[index];
  }

  public async downloadFile(index: number) {
    fetch(this.documents[index].documentUrl)
      .then((file) => file.blob())
      .catch(err => {
        console.log(err);
      });
  }
}
