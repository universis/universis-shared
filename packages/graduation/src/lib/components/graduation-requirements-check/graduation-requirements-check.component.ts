import { Component, OnInit } from '@angular/core';
// import { GraduationRequirement, GraduationRequirementType, GraduationItemStatus } from './../../graduation';

enum GraduationRequirementType {
  Simple = 'simple',
  LogicOr = 'or',
  LoginAnd = 'and',
}

enum GraduationItemStatus {
  Pending = 'pending',
  Completed = 'completed',
  Failed = 'failed',
  Unavailable = 'unavailable',
}

interface GraduationRequirement {
  title: String;
  description: String;
  type: GraduationRequirementType;
  status: GraduationItemStatus;
  subrules?: Array<GraduationRequirement>;
}


@Component({
  selector: 'lib-graduation-requirements-check',
  templateUrl: './graduation-requirements-check.component.html'
})
export class GraduationRequirementsCheckComponent implements OnInit {

  public totalStatus = 'loading';

  // Temporary, this will be "translated" from status
  public statusMessage = 'Γίνεται έλεγχος των προϋποθέσεων πτυχίου.';


  public requirements: Array<GraduationRequirement> = [
    {
      title: 'ΥΠΟΧΡΕΩΤΙΚΑ',
      description: 'O απαιτούμενος αριθμός μαθημάτων θα πρέπει να είναι μεγαλύτερος ή ίσος με 25.',
      type: GraduationRequirementType.Simple,
      status: GraduationItemStatus.Completed,
    },
    {
      title: 'ΥΠΟΧΡΕΩΤΙΚΑ ΚΑΤΕΥΘΥΝΣΗΣ',
      description: 'O απαιτούμενος αριθμός μαθημάτων θα πρέπει να είναι μεγαλύτερος ή ίσος με 16.',
      type: GraduationRequirementType.Simple,
      status: GraduationItemStatus.Completed,
    },
    {
      title: 'ΕΠΙΛΟΓΗΣ ΚΑΤΕΥΘΥΝΣΗΣ ΚΑΙ ΕΙΔΙΚΗΣ ΕΠΙΛΟΓΗΣ',
      description: 'Απαιτούνται τα δύο παρακάτω:',
      type: GraduationRequirementType.LoginAnd,
      status: GraduationItemStatus.Failed,
      subrules: [
        {
          title: 'ΕΠΙΛΟΓΗΣ ΚΑΤΕΥΘΥΝΣΗΣ ',
          description: 'O απαιτούμενος αριθμός μαθημάτων θα πρέπει να είναι μεγαλύτερος ή ίσος με 5.',
          type: GraduationRequirementType.Simple,
          status: GraduationItemStatus.Completed,
        },
        {
          title: 'ΕΙΔΙΚΗΣ ΕΠΙΛΟΓΗΣ ',
          description: 'O απαιτούμενος αριθμός μαθημάτων θα πρέπει να είναι μικρότερος ή ίσος με 3.',
          type: GraduationRequirementType.Simple,
          status: GraduationItemStatus.Failed,
        }
      ]
    },
    {
      title: 'Αριθμος ECTS η Αριθμος Διδακτικων Μοναδων',
      description: 'Απαιτείεται ένα απο τα δύο παρακάτω:',
      type: GraduationRequirementType.LogicOr,
      status: GraduationItemStatus.Completed,
      subrules: [
        {
          title: 'Αριθμος ECTS',
          description: 'O απαιτούμενος αριθμός ECTS θα πρέπει να είναι μεγαλύτερος ή ίσος με 240.',
          type: GraduationRequirementType.Simple,
          status: GraduationItemStatus.Completed,
        },
        {
          title: 'Αριθμος Διδακτικων Μοναδων',
          description: 'O απαιτούμενος αριθμός διδακτικών μονάδων θα πρέπει να είναι μικρότερος ή ίσος με 120.',
          type: GraduationRequirementType.Simple,
          status: GraduationItemStatus.Unavailable,
        }
      ]
    },
    {
      title: 'Εκπονηση Πτυχιακης Εργασιας',
      description: 'Θα πρέπει να έχει εκπονηθεί πτυχιακή εργασία',
      type: GraduationRequirementType.Simple,
      status: GraduationItemStatus.Pending,
    },
  ];


  constructor() { }

  ngOnInit() {
  }

}
