import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraduationRequestComponent } from './graduation-request.component';

describe('GraduationRequestComponent', () => {
  let component: GraduationRequestComponent;
  let fixture: ComponentFixture<GraduationRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraduationRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraduationRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
