import { Injectable } from '@angular/core';
import { StudentService } from './../student/student.service';
import { GraduationRequestStatus } from './../../graduation';
import { GraduationRequestStep } from './../../graduation-request-step';
import {
  GraduationRequestFormComponent
} from './../../components/graduation-request-form/graduation-request-form.component';
import {
  GraduationRequirementsCheckComponent
} from './../../components/graduation-requirements-check/graduation-requirements-check.component';
import {
  GraduationDocumentSubmissionComponent
} from './../../components/graduation-document-submission/graduation-document-submission.component';
import {
  GraduationCeremonyComponent
} from './../../components/graduation-ceremony/graduation-ceremony.component';

/**
 *
 * Handles data between api and the students app regarding the graduation process
 *
 */

@Injectable({
  providedIn: 'root'
})
export class GraduationRequestService {

  constructor(
    private _studentService: StudentService,
  ) { }

  /**
   *
   * Creates a bundle with information regarding the graduation process
   * Returns the basic overview of the component and not the data required from
   * the individual parts.
   *
   */
  async getGraduationRequestStatus(studentId: number): Promise<GraduationRequestStatus> {
    // available statuses: 'unavailable', 'available', 'submitted', 'pending'
    // TODO: Add the API calls

    let student: any;
    try {
      student = await this._studentService.getStudent(studentId);

    } catch (err) {
      // TODO: log the error
      console.error(err);
    }

    // if (!student || !student.department || !student.department.currentPeriod) {
    //   throw new Error('Could not get student information');
    // }

    // This calculation is expected be common
    const academicYear = '2019-2020';
    const graduationPeriodExist = true;
    const requestPeriodStart = new Date();
    const requestPeriodEnd = new Date();
    const now = new Date();
    const graduationRequestPeriodOpen = now >= requestPeriodStart && now <= requestPeriodEnd;

    return {
      graduationPeriodExist,
      academicYear,
      academicPeriod: 'summer', //''student.department.currentPeriod',
      requestPeriodStart,
      requestPeriodEnd,
      graduationRequestPeriodOpen,
      steps: [
        {
          title: 'Graduation Request',
          alternateName: 'graduationRequest',
          component: this.getComponent('graduationRequest'),
          status: 'available'
        }, {
          title: 'Requirements check',
          alternateName: 'graduationRequirementsCheck',
          component: this.getComponent('graduationRequirementsCheck'),
          status: 'pending'
        }, {
          title: 'Requirements check',
          alternateName: 'graduationDocumentsSubmission',
          component: this.getComponent('graduationDocumentsSubmission'),
          status: 'failed'
        }, {
          title: 'Requirements check',
          alternateName: 'graduationCeremony',
          component: this.getComponent('graduationCeremony'),
          status: 'unavailable'
        }
      ],
      graduationRequestStatus: 'available',
      prerequisitesCheck: 'available',
      documentSubmission: 'available',
      graduationCeremony: 'available'
    };
  }

  /**
   *
   * Creates a bundle withe the document submission statuses that the user is
   * required to submit
   *
   */
  async getGraduationDocumentsStatus(): Promise<any> { // TODO: change type

  }

  /**
   *
   * Resolves alternateNames to angular components
   *
   * @param alternateName The alternate name of the component
   *
   */
  getComponent(alternateName: string): GraduationRequestStep {
    switch (alternateName) {
      case 'graduationRequest': return new GraduationRequestStep(GraduationRequestFormComponent, {});
      case 'graduationRequirementsCheck': return new GraduationRequestStep(GraduationRequirementsCheckComponent, {});
      case 'graduationDocumentsSubmission': return new GraduationRequestStep(GraduationDocumentSubmissionComponent, {});
      case 'graduationCeremony': return new GraduationRequestStep(GraduationCeremonyComponent, {});
    }
  }
}
