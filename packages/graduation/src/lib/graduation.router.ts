import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GraduationRequestComponent } from './components/graduation-request/graduation-request.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'graduation-request',
        component: GraduationRequestComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GraduationRoutingModule {
  constructor() {}
}
